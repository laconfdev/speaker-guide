# Speakers Guide

This repository contains a copy of the Speakers Guide that is normally used at linux.conf.au.
The intent is to develop a standalone guide that is accessible for all LCAs, plus available for use at other conferences.

**Document Updates**

[05/01/2022] Updated licencing info, moved some ToC elements around

## Acknowledgements
Many thanks to past linux.conf.au and PyCon AU organising teams for their assistance with creating the content of this document.

## Congratulations!
Congratulations on being selected to present a talk at linux.conf.au 2022. We know this is a big undertaking on your part, so we want to help make this an enjoyable experience.

**NOTE:** Please ensure that you check this document frequently as information may change. A changelog will be provided at the top, so you can see if any changes have been made.

## Table Of Contents
- [TL;DR Important Points / Summary](#tldr-important-points-summary)
- [Stuff You Need To Know](#stuff-you-need-to-know)
  - [Conference Ticket](#conference-ticket) 
  - [Mailing List](#mailing-list)
  - [Online Presentation](#online-presentation)
  - [Time Zone](#time-zone)
  - [Tech Checks](#tech-checks)
  - [Arrive in the Backstage Platform Before Your Tech Check](#arrive-in-the-backstage-platform-before-your-tech-check)
  - [Conduct and Expectations](#conduct-and-expectations)
  - [Your Published Talk Details](#your-published-talk-details)
  - [What To Do If Something Comes Up](#what-to-do-if-something-comes-up)
- [Your Talk & Presentation](#your-talk-presentation)
  - [Live or Pre-Recorded?](#live-or-pre-recording-presentations)
  - [Live Presentations](#live-presentations)
  - [Pre-Recorded Presentations](#pre-recording-presentations)
  - [Talk Duration & Format](#talk-duration-format)
  - [Talk Recording & Publication](#talk-recording-publication)
  - [Talk Licence](#talk-licence)
- [How To Look and Sound Good In Your Talk](#how-to-look-and-sound-good-in-your-talk)
  - [Slides](#slides)
  - [Microphone/Audio Quality](#microphone-and-audio-quality)
  - [Cameras and Lighting](#cameras-and-lighting)
  - [Camera Positioning](#camera-positioning)
  - [Internet Connection](#internet-connection)
  - [Timer, Power and Second Monitor/Device](#timer-power-and-second-monitordevice)
- [The Tech Systems](#the-tech-systems)
  - [About Venueless](#about-venueless)
  - [About Backstage](#about-backstage)
  - [About StreamYard](#about-streamyard)
  - [About NDV Presenter Portal](#about-next-day-video-ndv-presenter-portal)
- [On The Day](#on-the-day)

## TL;DR Important Points Summary

- Check for an email from "Next Day Video Presenter Portal".
- Book a pre-conference [Tech Check](#tech-checks) via this portal ASAP.
- Join the conference [Backstage](#the-tech-systems) before your Tech Check (it's where you will do your Tech Check).
- Read & watch the [Tech Tips](#online-presentation) so you're ready to go.
- Pre-recorded sessions must be submitted by Dec 31, 2021.
- Be online **90 minutes before your talk on the day** (so we know you're online) by saying hello in #speaker-checkin.  You'll do another Tech Check 60 minutes before.
- Jump into [Venueless](#about-venueless) after your talk to connect with attendees.

[^Back to ToC](#table-of-contents)

## Stuff You Need To Know

### Conference Ticket
All primary speakers for the main conference presentation will now have access to a complimentary Speakers ticket for the conference. Additional speakers will need to purchase a ticket to the conference, but will have a discounted rate applied.

**Please note:** All speakers on your submission must register for the conference. If you have additional speakers, please ensure they have accepted your invitation to the proposal and have registered for a ticket.

To get your ticket:
1. Open your Dashboard
2. Navigate to Get Your Ticket
3. Choose your ticket type
    - Speaker ticket is available for primary speakers, and is free
    - Additional speakers will receive a discount on professional and contributor tickets
    - All speakers have the option to purchase a ticket as well, to contribute towards the running of the conference, especially for giving financial assistance to other speakers and attendees
4. Follow the steps to complete your registration

### Mailing List
All speakers will be subscribed to the speakers mailing list. This list will be used by the organising team to send out updates to everyone in the lead up to the conference. You may also use this mailing list to pose questions to the wider speaker group. We will try to keep emails to a minimum, and we ask that you take this into account when mailing the list also.

The email address for the mailing list is: [speakers@lists.lca2022.linux.org.au](mailto:speakers@lists.lca2022.linux.org.au)

Please consider taking appropriate actions to ensure emails sent from this address do not end up in your spam folder.

### Online Presentation
There are many things different about presenting this year.  These are the key things to remember:

- Watch our **[LCA Presenter Tech Tips video](https://youtu.be/DPKhhLxfllM)** and ensure your presentation area, camera, mics are optimal, your Internet is stable & you are ready for testing.  **At a minimum please use a headset USB microphone**, not your built-in laptop mic (which will slightly echo and be fatiguing for long talks). 
- Find a **secondary monitor** to plug in (or have a second laptop/device).  This is not mandatory, but it will make for easier communication when live presenting.
- Figure out if you want to **Live Streaming** (preferred, default) or **Pre-Recording**.
- Book a [Tech Check](#tech-checks) via the Next Day Video Presenter Portal before the event (limited dates). - You will be emailed a link to book.
- If you are **Pre-Recording**, submit your video on or before **Dec 31, 2021**, so we can review and approve it.
- Join our [Backstage](#the-tech-systems) platform

There are a few things you need to consider with your presentation:

- You will present remotely using Screen Sharing (full screen or window).  Slides should be in 16:9 format, 720p or 1080p resolution
- You should have a good quality webcam and mic (headset mic is OK, avoid using your built-in laptop mic).
- Connection to the stream will be via a web browser (WebRTC) to allow us to work with all OS’s. Google Chrome/Chromium is required (other browsers don’t work as well).
- You will be invited to the Backstage platform.  This will allow us to ping you prior to and during the conference and will be used for Tech Checks. Please ensure you have notifications turned on, so you hear the ping from us!
- Ensure that you watch the following [LCA Presenter Tech Tips video](https://youtu.be/DPKhhLxfllM). 

If you have any of the following, please contact the team at speakers@lca2022.linux.org.au:
- Other AV requirements beyond screen sharing, webcam, and audio.
- If you are planning on using OBS Virtual Camera or similar technologies (which is discouraged; we mix your screen/camera on our end)
- Limitations with streaming equipment or software

You will have 2 options available:
1. Pre-Recording
2. Live Streaming

For urgent matters during the week, please get in touch with our Speaker Liaison or via the AV helpdesk channel in the backstage platform.

### Time Zone
The conference will be run in the Canberra, ACT timezone Australian Eastern Daylight Time which is GMT+11.

You can check your local time with Canberra (same as Sydney) using this link [https://www.zeitverschiebung.net/en/timezone/australia--sydney](https://www.zeitverschiebung.net/en/timezone/australia--sydney). 

### Tech Checks

We require all presenters to have a Tech Check in advance of the conference, even if you’re pre-recording, or you've presented at other virtual events.

**Note: Our backstage has changed since 2021. The Tech Check will run you through this new platform, so you can familiarise yourself before the day of the conference.**

If you think you really don't need a tech check, please email [speakers@lca2022.linux.org.au](mailto:speakers@lca2022.linux.org.au) with your exemption reason.

The following tech check time slots are available (Melbourne GMT+11 timezone):
 * Fri 10th Dec:  2 pm - 6 pm    (14:00 - 18:00)
 * Fri 10th Dec:  9 pm - 1 am    (21:00 - 01:00)
 * Sat 11th Dec: 11 am - 5:30 pm (11:00 - 17:30)
 * Sun 12th Dec:  2 pm - 6 pm    (14:00 - 18:00)

Tech checks will give our AV team an opportunity to give you personalised advice on things like your webcam setup, microphones, lighting and slides, and make sure your setup works with our systems. It’s the perfect time to ask all the small questions you might have about presenting.

**You should do your Tech Check using the same laptop/device you will present from, using the same equipment in the same location (and at the same time of day if possible).**

At the time of your Tech Check, please make yourself known to AV staff in #tech-check - "Hello, I am here for my Tech Check".

### Arrive In the Backstage Platform Before Your Tech Check
Please make sure you know when and where your talk is scheduled in the local conference time zone (see above). Please check the [schedule](https://lca2022.linux.org.au/schedule/) to confirm the day, time, and room for which your talk is scheduled.

Since we are so close to the event, we will not be making any more schedule changes unless absolutely necessary. If any changes affect you, we will let you know as soon as possible.

Information on joining the [Backstage](#the-tech-systems) platform and what happens during your live presentation is below. 

### Conduct and Expectations
Please make sure that your talk content and slides comply with the linux.conf.au [Code of Conduct](https://lca2022.linux.org.au/attend/code-of-conduct/). We take our Code of Conduct extremely seriously and expect all of our speakers to uphold it on our stages.

An important note, particularly for those speaking on topics of security. We ask that, as you're planning your talks, you:

1. Do not include demos where you attack or penetrate-test the conference networks, conference infrastructure, conference processes, or the conference attendees in any way.
2. If talking about any past hacks or social engineering, be careful to note clearly in your talk that you had informed consent for any system or person affected.
3. Do not play copyrighted content you do not have permission to use (such as other people’s music or videos), especially things that will be picked up by Content ID.

Any actions that make attendees feel unwelcome or unsafe fall under the [Code of Conduct](https://lca2022.linux.org.au/attend/code-of-conduct/). If you have not read through this before, we encourage you to do this at your earliest convenience.

Please also include a content warning if your talk contains content that you think might be sensitive, distressing or traumatic for some people (even if most are ok with it). 
**Note that Content warnings do not exempt you from the Code of Conduct.**

### Your Published Talk Details
Your talk information and biography is [up on our website](https://lca2022.linux.org.au/schedule/).

Please take the time to look over your talk entry, and make sure that you are happy with your abstract and biography.

Should you wish to update your biography, profile photo, or abstract, please do the following:
1. Edit your Speaker Profile and/or your original talk proposal using the [LCA Dashboard](https://lca2022.linux.org.au/dashboard/).
2. Email the conference organisers at [speakers@lca2022.linux.org.au](mailto:speakers@lca2022.linux.org.au) to let them know, so the changes can be published to the schedule.

(Note, the LCA Dashboard is different to the Next Day Video Presenter Portal).

We advise you to send through any changes as soon as possible, ahead of the event, as the organising team will be very busy during the event and will not have time to push website changes except for emergencies. Our AV Team also relies on the data from the schedule in order to produce title slides and other information for videos uploaded to YouTube or on-screen information during the stream, so it is important to get this done as soon as possible.

### What To Do If Something Comes Up
We understand things don't always go to plan. Laptops run out of power, slides go missing, transport runs late, family stuff comes up, physical or mental health problems can occur.

If anything comes up at any point, please let one of our conference organisers know. We will be monitoring the speakers mailing list ([speakers@lists.lca2022.linux.org.au](mailto:speakers@lists.lca2022.linux.org.au)) as well as the speakers support queue ([speakers@lca2022.linux.org.au](mailto:speakers@lca2022.linux.org.au)) during the week. We have an experienced organising team who can help with a broad range of questions, issues, or concerns. The sooner you let us know if there is a problem, the sooner we can work with you to achieve the best possible outcome.

Your main contacts during the week will be Wil Brown. Wil is the speaker coordinator for LCA2022 and will look after any questions you have. You can contact him via [speakers@lca2022.linux.org.au](mailto:speakers@lca2022.linux.org.au). Miles Goodhew (Chair) and Neill Cox (Co-Chair) will also be on hand to help in any way.

If you have an issue that should involve our Conduct Team, a 24-hour hotline number will be made available at the event. Less immediate issues of Conduct can be emailed to [safety@lca2022.linux.org.au](mailto:safety@lca2022.linux.org.au). Signs with these details will be around the venue.

If you have an immediate safety concern, please contact Emergency Services (000 in Australia or your local country or region’s emergency number). 

[^Back to ToC](#table-of-contents)

## Your Talk & Presentation

### Live or Pre-Recording Presentations?
We would prefer you to present live, but we understand that isn’t always an option.

### Live Presentations
#### Testing Your Live Presentation
A [Tech Check](#tech-checks) is required and will be completed prior to the conference for you to check your mic, camera, presentation setup with our system. This will be simulating your LCA experience, allowing you to check that your work.

#### Key Tips
- We strongly encourage you to have a secondary monitor (or a secondary laptop) as it will make communicating with you during your talk much easier.
- Use a LAN connection if possible for the highest bandwidth capacity, Wi-Fi can be unreliable for live-streaming.
- Plug in everything to a mains outlet.  Batteries can sense when you are presenting live and will run out faster - scientific fact!  

### Pre-Recording Presentations
You may choose to pre-record your video if you don’t want your Internet connection causing any issues or if you have some interesting things you would like to do with your video. There will be submission requirements for your video.

**1. You must set your talk to "Pre-recorded" in the Next Day Video Presenter Portal by 10th December.**

**2. Due date for video uploads: December 31st (two weeks before the conference)**

If we do not receive a video by the due date, **you will default back to live-streaming**. 

All videos are **quality checked** and not accepted until we have reviewed, so we encourage you to upload **earlier** than the due date for feedback.

If you are co-presenting with another speaker, we do not accept a mix of live and pre-recorded presentations.  In this scenario, please make sure you submit one pre-recorded video which includes both presenters.

#### Technical Requirements
- Video length: The **maximum** duration is 30 minutes (miniconf talks) or 45 minutes (standard talks). It can run a little shorter, but it can not run longer, even by a second.
- Resolution:1080p (1920x1080) or 720p (1280x720)
- Avoid lots of audio ‘gain’ filters if your audio is too quiet.  This probably means you have a mic issue (see our **[Tech Tips](https://youtu.be/DPKhhLxfllM)** video), excessive software gain generally doesn't sound very good.
- **We request that you have a Tech Check before you pre-record.**
- Uploading video will be via the Next Day Video Presenter Portal.
- Recommend you start your video with introductory slides, with the name of the talk and who you are.  Leave these up for at least a few seconds.
- You must be available in the conference Backstage during your talk, so you can answer questions!
- Editing needs to be done by you, and requesting changes after the due date is unlikely. The AV team is expecting to have many hours of video to work through, so we likely will not have the capacity to do special pre-event edits ourselves. You can however request an extension.

#### Preferred Format
- Format: MP4 H.264 with AAC audio (Handbrake preset “YouTube HQ 1080 or 720”).
- Video Bitrate: At least 2.5 Mbit for 720p, 4 Mbit for 1080p.
- Audio Bitrate: At least 160 Kbit, mono or stereo.
- File size: Under 4 GB
- Framerate: 30 (but 29.97/30/25/24/50/60 OK too; make sure your [lights don't flicker](http://urbanvideo.ca/avoid-video-flicker))
- Where to upload to: Via Next Day Video Presenter Portal (URL will be emailed to you, check your inbox for "Next Day Video Presenter Portal").

If your format is a bit different to above (i.e. a MOV file), we would prefer to handle format conversion, so upload it anyway.

**NOTE:** If you have multiple presenters, please submit a single video - AV team has limited capacity for custom edits, we're expecting an edited video ready to broadcast.

#### Pre-recording video with OBS (Open Broadcast Studio)

Next Day Video has produced a video to help you pre-record your video using OBS - [Using OBS to record your presentation - YouTube](https://www.youtube.com/watch?v=byGwbJEHXj4)

### Talk Duration & Format
Standard length talks for the main conference run for **45 minutes, including questions**.

Standard length talks for Miniconf run for **30 minutes, including questions**.

Before your talk starts, an LCA MC will introduce you on the live stream, and when it ends, they will thank you.  The experience will be fairly seamless -you will be able to talk to the MC before your talk starts without it being streamed out, and we will make sure you are ready to go before we go live.

You are in charge of allowing time for questions inside your time slot. Often, speakers who choose to take questions will allow 5 minutes for questions.

However, you can decide how you would like to handle questions about your talk. By default, MC's will assume you are not taking questions. Please tell your MC what your preferences are.

You have two choices for taking questions for your talk:
1. **No questions** - you can use the whole duration for your talk. If people have questions they can find you afterwards.
2. **Curated questions only** - your MC will monitor the chat during the talk and receive questions from the audience.

Your MC will sit with you in the stream and give you a stopwatch timer link to prompt you to keep track of your time. Please speak with your MC before your session begins about any preferences you have for this.

There are 10 minutes between the talks for changeover. Please respect this time limit. Once your slot is over, you may answer questions in the Venueless platform after the session.

### Talk Recording & Publication
Your talk will be recorded, unless you have opted out of the recording release in your talk proposal form. All recorded talks will be available on the [linux.conf.au YouTube channel](https://www.youtube.com/user/linuxconfau) shortly after the event. Talks will be streamed into the Conference Platform during the conference.

### Talk Licence
We will publish these talks under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

What does this licence mean?

- Attribution (BY): you must give the original speaker credit.
- Non-Commercial (NC): no making profit off the content through advertising, charging money to see it, etc.
- Share-Alike (SA): any redistributed alterations of the content must also have this licence.

What we intend this licence to allow:

- Public and private re-showing of content, e.g at work, at meetups.
- Speakers to re-host their own personal content, even to other services.
- Excerpts in NC-compliant podcasts (i.e. no profit is made from the podcast)
- NC-compliant educational purposes (i.e. no profit is made from sharing the content, and no fee is charged to access it)
- Inspiration for future talks (as a source of ideas, but not to enable plagiarism)

We will be marking all uploaded videos with the YouTube Standard licence metadata tag to prevent content scrapers from automatically re-uploading the talks we publish. However, we will use the video description to clarify to human readers that the licence applicable to the video is CC BY-NC-SA 4.0.


[^Back to ToC](#table-of-contents)

## How To Look and Sound Good In Your Talk
### Slides
Here are some suggestions for slide design that will help you avoid a few common disappointing situations that presenters can face. (We thank previous PyCon AU organisers for this helpful list).

- Ensure that text isn’t too close to the borders or sides of your slides
    - Avoid highly indented bullet points
        - It’s a sign you’re overloading the slide.
        - With too much information.
            - Which can get frustrating
- Ensure that text is large enough to be readable at a distance (> 18 point is usually a good guide). If you can read your slide in the thumbnail preview, that is a good sign.
- Many people have problems with seeing low contrast colours and images. Please also try to consider colour-blind people when picking colours.
- Spellcheck your presentation.
- Consider using a slide lint tool, such as [slidelint](https://github.com/enkidulan/slidelint), to check your slides for many of the above conditions.

### Microphone and Audio Quality
By far, the best investment is ensuring audio quality is clear and not echo-y. 

**This is more important than video quality.**

Using your laptop microphone is OK for short talks or video chat, but the echo will get very fatiguing to listen to for a 20+ minute talk.

In an ideal world; an external USB condenser microphone is your best bet, but you will need to practice with it to ensure you’re getting optimal audio levels, it’s positioned correctly, it doesn't accidentally pick up your speakers/other sounds during your talk, and make sure any gain or volume controls are set, so it's loud and clear, without any distortion, even when you're speaking loudly into it.  

**If you’re buying an external microphone, invest the time to learn how to use it.**

Many podcasting guides are very useful to read, for example: [https://www.buzzsprout.com/blog/mic-technique-podcasting](https://www.buzzsprout.com/blog/mic-technique-podcasting).

Dropping from this, using a USB headset with a microphone is the next best thing (and super easy).

Practice by recording some audio in something like Audacity, and listen to the difference in audio quality between your laptop microphone or the positioning of the microphone. Pay attention to the audio control panel in your OS to ensure you are positioned in front of the microphone correctly (if you are using an analog microphone).

When presenting, try to isolate yourself away from any background/ambient noise (close windows and doors, turn off fans, phone on vibrate, away from people if possible, etc.).

### Cameras and Lighting
Some laptop webcams work pretty well (i.e. those in a MacBook); others aren't so good. An external USB webcam such as a Logitech C920 is a relatively affordable way to add high quality video.

That said, before you do that, make sure you’re in a well lit position - it’s surprising how much difference this makes to quality. Lighting should be in front of you (facing back towards you), with no visible lights behind you.

An easy/cheap way to ensure you’re well lit is to sit facing a window: but keep in mind the time of day when you test, compared to the time of day when you are presenting.

### Camera Positioning
Make sure you are filling the frame - you don’t want your head at the bottom of the shot with lots of empty space!

If you can (especially if you have an external webcam and microphone), present standing up rather than sitting down. May sound silly, but it will help considerably with your presence. If you do this, ensure your webcam is level or facing down at you, though, rather than pointing up (a stand may help).

If you’re feeling particularly creative, you could even consider a more professional setup with a dedicated HDMI camera and tripod, wireless lapel microphone, lighting and backdrop, but a decent external webcam and external microphone can get you most of the way there.

###Internet Connection
If you can, a wired Internet connection is going to beat the pants of a wireless connection. Latency/jitter/dropouts (which are more likely to occur on Wi-Fi) especially affect video streaming.

Streaming at 720p ideally requires over 2 Mbit, with a minimum of 1 Mbit for OK quality. 5 Mbit or higher is highly recommended (i.e.a NBN 25/5 connection or higher). When you are presenting, strong recommendation to ensure nobody else in your household is utilising upload bandwidth (i.e. peer to peer applications).

### Timer, Power and Second Monitor/Device
Having a timer displayed (on a second monitor, device, phone) with a countdown will help you keep on time.  We can supply you a countdown link before your talk.

If you’re streaming, and you’re using speaker notes, keep in mind you’ll also need to keep the relevant chat channel / stream feed back to you open, so you can be communicated with during your talk if required. It is possible you may need a third monitor or another device if your speaker notes take over the other screen, but testing/practice will reveal this.

Ensure all devices you are using are powered, and any settings such as automatic screen sleep, pop-up notifications or screen savers are turned off.

[^Back to ToC](#table-of-contents)

## The Tech Systems
- Venueless: On-line conference attendee platform (Open Source)
- Rocket.Chat: The backstage platform (Open Source)
- StreamYard: Live broadcast platform (Proprietary)
- Next Day Video Presenter Portal: Presenter portal (Open Source)

### About Venueless
Our online conference platform is called Venueless.  This is where all the attendees will be able to watch your presentation.

You can launch Venueless from your LCA dashboard, by clicking the “Launch Conference” button.

![conference-launch-button](assets/img/launch-conference-button.jpg)

Jump into the "#Rego Desk" channel when you enter the conference platform.

![venueless](assets/img/lca-venueless.jpg)

### About Backstage
Our "backstage" platform is the open-source rocket.chat application.  This is where conference organisers, AV technicians and speakers will chat and coordinate the logistics for the conference presentations.

You will need to log in to the backstage platform for your pre-conference Tech Check as well as T-90, T-60 and T-10 min procedures on the day.

Conference organisers will make sure you are ready for your presentation in this backstage platform.

Log in to our backstage at https://backstage.linux.conf.au/.

Click the "LCA Login" blue button.

![rocket.chat-1](assets/img/rocketchat-login-1.jpg)

This will redirect you to your LCA Dashboard login.

Enter the username and password you registered with LCA into the login prompt.

![rocket.chat-2](assets/img/rocketchat-login-2.jpg)

You will be redirected and automatically logged into the backstage area.

![rocket.chat-3](assets/img/rocketchat-login-3.jpg)

If the web browsers asks for permission to send you notifications, can you please allow this feature.

At the time of your Tech Check, please make yourself known to AV staff in #tech-check - "Hello, I am here for my Tech Check".

#### Backstage Channels
- &#35;speakers-lounge: Speakers can to each other before, during and directly after the conference.
- &#35;speaker-checkin: On the day of the conference, announce your arrival in this channel 90 mins before your talk.
- &#35;tech-check: Used to coordinate Tech Checks during your pre-event booked session, and 60 mins before your presentation.
- &#35;green-room-{kaya, kia-ora, wominjeka, yuma}: Used to coordinate getting you into the streaming platform 10 mins before your presentation.


### About StreamYard
Our conference live-streaming platform is StreamYard.  This is where you will be presenting your talk.

Before your live presentation is about to start you will be sent a link to the StreamYard broadcast studio where you will perform your final quick tech check.

The MC and AV technician will make sure everything is ready for you to present.

![streamyard](assets/img/streamyard.jpg)

### About Next Day Video (NDV) Presenter Portal
The NDV Presenter Portal allows speakers to book their pre-conference tech check and to choose your presenting method; live talk or pre-recorded video.

![NVD-presenter-portal](assets/img/Next-Day-Video-AU-Presenter-Portal-for-LCA-2022-Online.jpg)

Please pay attention to the booking times.  They are in the same timezone as the conference, Melbourne GMT+11.

[^Back to ToC](#table-of-contents)

## On The Day

### 90 Minutes Before Your Live Broadcast (T-90)
Ninety minutes before you are due to present, login to the backstage platform and say "Hello" or "I'm here" in the #speaker-checkin channel.  

Organisers will mark you as present and add you to the tech check queue.

Stay logged in and keep the backstage platform open and visible so that you can see and hear notifications. 

### 60 Minutes Before Your Live Broadcast (T-60)
Sixty minutes before you are due to present, you will be pinged in the backstage platform and moved to the T-60 AV tech check area.

An AV technician will run through your setup making sure everything is of the highest quality for your live broadcast. 

Feel free to ask any technical questions here.

### 10 Minutes Before Your Live Broadcast (T-10)
Ten minutes before you are due to present, you will be pinged in the backstage platform (in your Green Room, matching the name of the conference program) and moved to the live broadcast studio.

An AV technician will perform a very quick tech check to make sure your setup ready to broadcast.

You'll also meet the room MC who will confirm your talk title, name and bio and answer any questions you have before you go live.

The MC will also ask if you are taking any live questions from the attendees after your presentation.

There will be a link for you to use a virtual timer/countdown so that you can stick to your allocated presentation time slot.

If you are taking live questions at the end of your presentation, make sure you leave some time within your allocated slot and before the countdown timer reaches zero.

### Live Broadcast Time (T-0)
The "virtual curtain" will go up on the live broadcast stream.

The room MC will appear on camera, introduce you and your talk, then hand the live broadcast over to you.

You and your presentation appear on camera, streaming live to the conference attendees.

Take a deep breath and have fun!

### After Your Presentation Is Finished
Once your talk is finished the MC will appear on screen with you and field questions from the attendees if there is time and if you have indicated this to the MC.

When the live broadcast time slot is up, or there are no more questions, the MC will thank you and the "virtual curtain" will go down on the live broadcast.

### After The Live Broadcast
Once your live broadcast has finished, and you are no longer on camera, we encourage you to log back in to Venueless and jump into the room you presented in.

Connect with and say hi the attendees.  You can answer any questions they have in more detail here.

Well done!  You have just presented at LCA2022 🤩

[^Back to ToC](#table-of-contents)

